package CARD_API;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import DATA_DRIVEN.UTILITY_CLASS;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class get_card_details {

	

	
	@Test(dataProvider = "get")
	
	void getd(String key,String command, String val1) {
		RestAssured.baseURI = "https://info.payu.in/merchant/postservice.php/?form=2";
		String input=key+"|"+command+"|"+val1+"|"+"1b1b0";
		String hashval = UTILITY_CLASS.encryptThisString(input);
		System.out.println("string  is "+input);
		System.out.println("hashval is "+hashval);

    Response response=RestAssured.given()
   .param("key",key)
   .param("command",command)
   .param("hash", hashval)
   .param("var1", val1)
   .post()
   .then()
   .statusCode(200)
   .header("Content-Type", "text/html; charset=UTF-8")
   .log().all()
   .extract()
   .response();
    String j=	response.asString();
    Assert.assertEquals(j.contains("Cards fetched Succesfully"), true);
    Assert.assertEquals(response.getTimeIn(TimeUnit.SECONDS)<5,true);
 
	System.out.println(response.getBody().asString());

	}
	@DataProvider(name = "get")
	String[][] insertdata() throws IOException {
		

		  String path="./RESOURCES/CARD_API.xlsx";
		  FileInputStream file= new FileInputStream(path);
		  XSSFWorkbook workbook= new XSSFWorkbook(file);
		  XSSFSheet sh =workbook.getSheet("sheet2");
		  int rc=sh.getLastRowNum();
		  int cc= sh.getRow(0).getLastCellNum();
		  String data[][] = new String[rc][cc];
		  for(int i=1;i<=rc;i++)
		  {
			  XSSFRow row =sh.getRow(i);
			  for (int j1=0;j1<cc;j1++)
			  {
				  String val=row.getCell(j1).toString();
				  System.out.println("val is "+val); 
			      data[i-1][j1]=val;   
			  
			  }
			  System.out.println("\n");
			  
		  }

			

		return data;	
	

}

}
