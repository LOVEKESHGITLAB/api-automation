package CARD_API;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.instrument.IllegalClassFormatException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import DATA_DRIVEN.UTILITY_CLASS;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class insert_card_details {
	static int i=1;
	
	@Test(dataProvider = "insert")
	public void test (String key,String command, String val1,String val2,String val3,String val4, String val5,String val6,String val7,String val8) throws IOException, IllegalClassFormatException
	{
		
		
		//String input = "smsplus|get_user_cards|smsplus:joker|1b1b0";
		String input=key+"|"+command+"|"+val1+"|"+"1b1b0";
		String hashval = UTILITY_CLASS.encryptThisString(input);
	//	System.out.println("card no is "+val6);
	//	System.out.println("input is "+input);
	RestAssured.baseURI="https://info.payu.in/merchant/postservice.php/?form=2";
	Response response=RestAssured.given().urlEncodingEnabled(true)
	.param("form", "2")
	.param("key", key)
	.param("command",command)
	.param("hash", hashval)
	.param("var1",val1)
	.param("var2",val2)
	.param("var3",val3)
	.param("var4",val4)
	.param("var5",val5)
	.param("var6",val6)
	.param("var7",val7)
	.param("var8",val8)
	.post()
	.then()
	.statusCode(200)
	//.log().all()
	.header("Content-Type", "text/html; charset=UTF-8")
	.extract()
	.response();
    String j= response.asString();
    Assert.assertEquals(j.contains("Card Stored Successfully."), true);
    Assert.assertEquals(response.getTimeIn(TimeUnit.SECONDS)<5,true);

	System.out.println(response.asString());
	String token=response.getBody().jsonPath().get("cardToken");
	System.out.println("token is "+token);	
	String path="./RESOURCES/CARD_API.xlsx";
	  FileInputStream file= new FileInputStream(path);
	  XSSFWorkbook workbook= new XSSFWorkbook(file);
	  XSSFSheet sh =workbook.getSheet("sheet3");
	//  int rc=sh.getLastRowNum();
	//  int cc= sh.getRow(0).getLastCellNum();
	 // cc=cc-1;
	  sh.createRow(i).createCell(0).setCellValue(key);
	  sh.getRow(i).createCell(1).setCellValue(command);
	  sh.getRow(i).createCell(2).setCellValue(val1);
	  sh.getRow(i).createCell(3).setCellValue(token);

	  	
	  FileOutputStream fout=new FileOutputStream(path);
		workbook.write(fout);
		workbook.close();
		file.close();
	System.out.println("i is "+i); 
    i++;
//	cc--;
	}
	@DataProvider(name = "insert")
	String[][] insertdata() throws IOException 
	{
		// String data[][]= {{"a","b"},{"c","d"}};
		
		 String path
		 ="./RESOURCES/CARD_API.xlsx";
		int rowc = UTILITY_CLASS.getRowCount(path, "sheet1");
		int colc = UTILITY_CLASS.getCellCount(path, "sheet1", 1);
		String data[][] = new String[rowc][colc];
		for (int i = 1; i <= rowc; i++) {
			for (int j = 0; j < colc; j++) {
				data[i - 1][j] = UTILITY_CLASS.getCellData(path, "sheet1", i, j);
			}

		}
return data;	
	

}

}