package CARD_API;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

import java.io.IOException;
import java.lang.instrument.IllegalClassFormatException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import DATA_DRIVEN.UTILITY_CLASS;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class delete_card_details {
	


	@Test(dataProvider = "delete")
	public void test (String key,String command, String val1,String val2) throws IOException, InvalidFormatException
	
	{	
	String input=key+"|"+command+"|"+val1+"|"+"1b1b0";
	String hashval = UTILITY_CLASS.encryptThisString(input);

	RestAssured.baseURI="https://info.payu.in/merchant/postservice.php/?form=2";
	Response response=RestAssured.given().urlEncodingEnabled(true)
    .param("key", key)
	.param("command",command)
	.param("hash", hashval)
	.param("var1", val1)
	.param("var2", val2)
    .post()
	.then()
	.statusCode(200)
	.extract()
	.response();
	 String j=	response.asString();
	 Assert.assertEquals(j.contains("card deleted successfully"), true);
	 Assert.assertEquals(response.getTimeIn(TimeUnit.SECONDS)<5,true);

	 System.out.println("response is"+response.asString());
	 
	}
	@DataProvider(name = "delete")
	String[][] insertdata() throws IOException {
		
		
		 String path
		 ="./RESOURCES/CARD_API.xlsx";
		int rowc = UTILITY_CLASS.getRowCount(path, "sheet3");
		int colc = UTILITY_CLASS.getCellCount(path, "sheet3", 1);
		String data[][] = new String[rowc][colc];
		for (int i = 1; i <= rowc; i++) {
			for (int j = 0; j < colc; j++) {
				data[i - 1][j] = UTILITY_CLASS.getCellData(path, "sheet3", i, j);
			}

		}

		return data;	
	

}



}
